﻿import pygame,sys
from pygame.locals import *
pygame.init()
X,Y=1024,704
fen = pygame.display.set_mode((1024,704))
fen.fill((255, 255, 255))
def maximum_lignes(Y,taille_police,interligne): # Calcule le nombre de lignes maximum dans un cadre
    Y_ligne=taille_police*0.722+interligne #nombre de pixels que prend une ligne
    nb_ligne_max=int((Y/Y_ligne))-1 #Calcule la valeur cherchée
    return nb_ligne_max #Retourne

def caractere_par_ligne(X,taille_police):#Calcule le nombre maximum de caractères sur une ligne
    X_caractere=taille_police*0.395 #Taille moyenne prise par un caractère (en fonction de la police utilisée)
    nb_caractere_max=int(X/X_caractere) #Calcule la valeur cherchée
    return nb_caractere_max #Retourne
def generation_texte(texte,taille_police,X): #Formate le texte à afficher
    nb_caractere_max=caractere_par_ligne(X,taille_police) #Nombre de caractère max (à l'aide des fonctions auxiliaires)
    x=0
    nouveau_texte=[] #Liste vide
    activateur=False #Variable
    for i in range(len(texte)-1): #Parcours le texte entré
        if i%nb_caractere_max==0 and i!=0: #Active "l'activateur" si position multiple du nombre maximum de caractères maximum
            activateur=True
        if texte[i-1]==' ' and activateur==True: #Si activateur=1 et si le caractère de la liste est un espace
            memoire="".join(texte[x:i-1]) #Extrait une partie du texte
            nouveau_texte.append(memoire) #...et l'ajouter à la liste sortante
            activateur=False #Désactive l'activateur
            x=i #Isole la prochaine partie du texte à traiter
    memoire="".join(texte[x:]) #Extrait la dernière partie du texte non traitée
    nouveau_texte.append(memoire) #...Et l'ajoute à la liste sortante
    return nouveau_texte #Renvoie la liste sortante

def affichage_texte(posX,posY,X,Y,centre,nouveau_texte,taille_police,couleur_texte,interligne,presence_fond,couleur_fond,fen):
#Fonction principale qui permet d'afficher un texte
    nb_ligne_max=maximum_lignes(Y,taille_police,interligne) #Calcule le nombre de ligne maximum (fonction auxilière)
    Y_ligne=int(taille_police*0.722+interligne) #Calcule le nombre de pixels pris par une ligne (fonction auxilière)
    if len(nouveau_texte)>nb_ligne_max: #S'il y a plus de lignes à afficher que ce qui est possible de faire
        return False #...Arrête la fonction

    else: #Si la résolution est assez grande pour afficher tout le texte
        if presence_fond==True: #Si un fond est désiré, on l'affiche à l'écran
            if not centre:
                pygame.draw.rect(fen,couleur_fond,(posX,posY,X,Y))
            else:
                pygame.draw.rect(fen,couleur_fond,(posX-X/2,posY-Y/2,X,Y))
        font=pygame.font.Font(None,taille_police)
        i=0
        if not centre: #Si on ne veut pas que le texte soit centré
            for l in nouveau_texte: #On affiche toutes les phrases à l'emplacement voulu
                texte=font.render(l,True,couleur_texte)
                fen.blit(texte,(posX,posY))
                posY+=Y_ligne
        else: #Si on veut que le texte soit centré
            posY=int((posY-((len(nouveau_texte)-1)/2)*Y_ligne)+interligne) #Définie les coordonnées de la première phrase
            for l in nouveau_texte: #On affiche toutes les phrases à l'emplacement voulu
                texte=font.render(l,True,couleur_texte)
                fen.blit(texte,(posX-texte.get_width()//2,posY-texte.get_height()))
                posY+=Y_ligne