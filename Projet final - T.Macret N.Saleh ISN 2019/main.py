﻿import pygame,sys,random,classes,gestionetage,texte #on importe tous les modules (3 de python, les autres de nous)
from pygame import *
from classes import *
from texte import *
from gestionetage import*
pygame.init()

"""création de la base et de quelques méthodes"""
#création de la fenetre
fen = pygame.display.set_mode((1024,704))
pygame.display.set_caption("Koya")
decor = pygame.image.load("sprites/decor/desert/desert1.png")
fen.blit(decor,(32,50))
tempsecoule = 0 #variables servant d'horloge (voir plus bas)
kd,ka,ks,kw = 0,0,0,0 #variables pour les tests claviers
lancement=0 #variable pour déterminer si l'on se trouve dans le menu ou non

#Importation des images du menu
menu=pygame.image.load("Menu/menu1.png")
fleche=pygame.image.load("Menu/fleche.png")
ecran_selection=pygame.image.load("Menu/ecran_tuto.png")
ecran_credits = pygame.image.load ("Menu/credits.png")
histoire = pygame.image.load ("Menu/histoire.png")
portailennemi=pygame.image.load("sprites/personnages/portailE.png")

#Importation des sons
son_valide = pygame.mixer.Sound("sons/entree.wav")
son_retour= pygame.mixer.Sound("sons/retour.wav")
son_touche= pygame.mixer.Sound("sons/degat1.wav")

#création des entitées, fonctions et listes de base pour le jeu
etage=Etage()
j1=Joueur(600,464,32,32,"joueur",150,150,.6,10,9)
ennemi0,ennemi1,ennemi2,ennemi3,ennemi4,ennemi5,ennemi6,ennemi7=0,0,0,0,0,0,0,0 #création des 16 emplacements à ennemis
ennemi8,ennemi9,ennemi10,ennemi11,ennemi12,ennemi13,ennemi14,ennemi15=0,0,0,0,0,0,0,0
mur0,mur1,mur2,mur3,mur4,mur5,mur6,mur7,mur8,mur9,mur10,mur11,mur12,mur13,mur14,mur15,mur16,mur17,mur18,mur19=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
mur20,mur21,mur22,mur23,mur24,mur25,mur26,mur27,mur28,mur29,mur30,mur31,mur32,mur33,mur34,mur35,mur36,mur37,mur38,mur39=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
mur40,mur41,mur42,mur43,mur44,mur45,mur46,mur47,mur48,mur49,mur50,mur51,mur52,mur53,mur54,mur55,mur56,mur57,mur58,mur59=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
mur60,mur61,mur62,mur63,mur64,mur65,mur66,mur67,mur68,mur69,mur70,mur71,mur72,mur73,mur74,mur75,mur76,mur77,mur78,mur79=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
mur80,mur81,mur82,mur83,mur84,mur85,mur86,mur87,mur88,mur89,mur90,mur91,mur92,mur93,mur94,mur95,mur96,mur97,mur98,mur99=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
bordure1=Mur(0,0,1024,50,"HUD/barreh",1)
bordure2=Mur(0,626,1024,78,"HUD/barreb",1)
bordure3=Mur(0,0,32,704,"HUD/barrec",1)
bordure4=Mur(992,0,32,704,"HUD/barrec",1)
Lpj =[j1]
Lpnj = [ennemi0,ennemi1,ennemi2,ennemi3,ennemi4,ennemi5,ennemi6,ennemi7,ennemi8,ennemi9,ennemi10,ennemi11,ennemi12,ennemi13,ennemi14,ennemi15]
Lmurs = [bordure1,bordure2,bordure3,bordure4,mur0,mur1,mur2,mur3,mur4,mur5,mur6,mur7,mur8,mur9,mur10,mur11,mur12,mur13,mur14,mur15,mur16,mur17,mur18,mur19]
Lmurs+=[mur20,mur21,mur22,mur23,mur24,mur25,mur26,mur27,mur28,mur29,mur30,mur31,mur32,mur33,mur34,mur35,mur36,mur37,mur38,mur39]
Lmurs+=[mur40,mur41,mur42,mur43,mur44,mur45,mur46,mur47,mur48,mur49,mur50,mur51,mur52,mur53,mur54,mur55,mur56,mur57,mur58,mur59]
Lmurs+=[mur60,mur61,mur62,mur63,mur64,mur65,mur66,mur67,mur68,mur69,mur70,mur71,mur72,mur73,mur74,mur75,mur76,mur77,mur78,mur79]
Lmurs+=[mur80,mur81,mur82,mur83,mur84,mur85,mur86,mur87,mur88,mur89,mur90,mur91,mur92,mur93,mur94,mur95,mur96,mur97,mur98,mur99]
Lprojectiles0, Lprojectiles1 = [],[]


j1.sort1=Sort(11,10,10,"11x",15,10,15,35,0,1)
j1.sort2=Sort(12,6,6,"12x",8,3,28,20,0,3)
j1.sort3=Sort(13,16,16,"13x",21,17,7,70,0,1)
j1.sort4=Sort(14,8,8,"14x",5,3,20,30,2,1)
j1.sort5=Sort(15,8,8,"15x",4,4,12,12,0,1)
j1.sort6=Sort(16,12,12,"16x",12,9,13,50,3,1)
j1.Lsorts = [0,0,0,0] #liste pour savoir quels sorts sont disponibles
j1.sortactuel=1

#actualise les listes d'entitées puis les entitées (et leurs hitbox) en elle mêmes
def actualisation_ig():
    fen.blit(decor,(32,50))
    etage.actuetage(j1,Lpnj,Lmurs)
    j1.actuprojectiles()
    Lprojectiles0=j1.Lprojs
    Lprojectiles1=[]
    for i in range (len(Lpnj)):
        if Lpnj[i]!=0: #si l'ennemi existe
         Lprojectiles1+=Lpnj[i].Lprojs #on ajoute les projectiles de l'ennemi à la liste de tout les projectiles
    for i in range (len(Lprojectiles0)): #destruction des projectiles qui sont dans des murs solides (joueur)
        for j in range (len(Lmurs)):
         if Lprojectiles0[i]!=0 and Lmurs[j]!=0:
          if Lmurs[j].solidite!=0 and Lprojectiles0[i].rect.colliderect(Lmurs[j]):
           Lprojectiles0[i]=0
    for i in range (len(Lprojectiles1)): #destruction des projectiles qui sont dans des murs solides (ennemis)
        for j in range (len(Lmurs)):
         if Lprojectiles1[i]!=0 and Lmurs[j]!=0:
          if Lmurs[j].solidite!=0 and Lprojectiles1[i].rect.colliderect(Lmurs[j]):
           Lprojectiles1[i]=0
    Lentitees=Lmurs+Lpnj+Lpj+Lprojectiles0+Lprojectiles1 #ordre d'actualisation : fond,murs,ennemis,joueur,projectiles
    for i in range (len(Lentitees)):
        if Lentitees[i]!=0:
            Lentitees[i].actuetblit() #on actualise touts les ennemis
    if etage.tpsapparition!=0: #si les ennemis ne sont pas actifs
        for i in range (len(Lpnj)):
         if Lpnj[i]!=0: #on cherche les ennemis existants
          fen.blit(portailennemi,(Lpnj[i].x,Lpnj[i].y))#on affiche un portail pour indiquer au joueur l'edroit où l'ennemi va apparaître
    j1.actusorts()
    j1.actubarre(j1.vie,j1.viemax,"vie",5,5)
    texte.affichage_texte(50,5,320,320,0,[str(j1.vie)+"/"+str(j1.viemax)],24,(255,255,255),0,0,(0,0,0),fen)
    j1.actubarre(j1.mana,j1.manamax,"mana",5,30) # puis en dernier viens l'actualisation du HUD
    texte.affichage_texte(50,30,320,320,0,[str(int(j1.mana))+"/"+str(int(j1.manamax))],24,(255,255,255),0,0,(0,0,0),fen)
    j1.actubouclier() #et le bouclier
    pygame.display.update()

#detection des collisions avec ennemis (+ detection de la mort)
def collision_ennemi():
    for i in range (len(Lpnj)): #pour chaque ennemi
     if Lpnj[i]!=0: #si l'ennemi existe
      for j in range (len(Lpnj[i].Lprojs)): #pour chaque projectile de l'ennemi
       if Lpnj[i].Lprojs[j] != 0: #si il existe
        if Lpnj[i].Lprojs[j].rect.colliderect(j1): #teste collision
         if j1.invu==0 and j1.bouclier!=1: #teste si le joueur peut être touché
          j1.vie-=Lpnj[i].Lprojs[j].degats #prise de dégats
          son_touche.play(0) #son de dégat
          j1.invu=20 #invulnérabilité de 1s
          j1.x+=Lpnj[i].Lprojs[j].dx*Lpnj[i].Lprojs[j].degats #on fait reculer le joueur (knockback) en s'assurant qu'il ne rentre pas dans un mur
          j1.rect=pygame.Rect(j1.x,j1.y,j1.largeur,j1.longeur) #(c'est le même principe que pour un déplacement)
          if j1.collisionmur(Lmurs)==1:
            j1.x-=Lpnj[i].Lprojs[j].dx*Lpnj[i].Lprojs[j].degats
          j1.y+=Lpnj[i].Lprojs[j].dy*Lpnj[i].Lprojs[j].degats
          j1.rect=pygame.Rect(j1.x,j1.y,j1.largeur,j1.longeur)
          if j1.collisionmur(Lmurs)==1:
            j1.y-=Lpnj[i].Lprojs[j].dy*Lpnj[i].Lprojs[j].degats
         Lpnj[i].Lprojs[j]=0 #destruction du projectile
      if Lpnj[i].rect.colliderect(j1): #collision avec l'ennemi, pricipe identique à celui avec un projectile
       if j1.invu==0 and j1.bouclier!=1:
        j1.vie-=Lpnj[i].dmgc
        son_touche.play(0)
        j1.invu=20
        j1.x+=Lpnj[i].dx*Lpnj[i].dmgc
        j1.rect=pygame.Rect(j1.x,j1.y,j1.largeur,j1.longeur)
        if j1.collisionmur(Lmurs)==1:
         j1.x-=Lpnj[i].dx*Lpnj[i].dmgc
        j1.y+=Lpnj[i].dy*Lpnj[i].dmgc
        j1.rect=pygame.Rect(j1.x,j1.y,j1.largeur,j1.longeur)
        if j1.collisionmur(Lmurs)==1:
         j1.y-=Lpnj[i].dy*Lpnj[i].dmgc
    if j1.invu>0:
     j1.invu-=1 #si le joueur est sous invulnérabilité, le temps d'invulnérabilité diminue
    if j1.vie<=0: #quand le joueur n'a plus de vie, c'est game over!
     print("Partie Terminée!")
     pygame.quit()
     sys.exit()

#detection des collisions sur les ennemis
def collision_joueur():
    for i in range (len(j1.Lprojs)):
     for j in range (len(Lpnj)):
      if Lpnj[j]!=0 and j1.Lprojs[i]!=0 : #si l'ennemi et le projectile existe
       if Lpnj[j].rect.colliderect(j1.Lprojs[i]): #si collision avec projectile
        Lpnj[j].vie-=j1.Lprojs[i].degats #inflige les dégâts du projectile
        Lpnj[j].tempstir=0 #on annule l'attaque actuelle de l'ennemi
        Lpnj[j].x+=j1.Lprojs[i].dx*j1.Lprojs[i].degats #on gère le recul, sans pour autant faire rentrer dans un mur
        Lpnj[j].rect=pygame.Rect(Lpnj[j].x,Lpnj[j].y,Lpnj[j].largeur,Lpnj[j].longeur)
        if Lpnj[j].collisionmur(Lmurs)==1:
            Lpnj[j].x-=j1.Lprojs[i].dx*j1.Lprojs[i].degats
        Lpnj[j].y+=j1.Lprojs[i].dy*j1.Lprojs[i].degats
        Lpnj[j].rect=pygame.Rect(Lpnj[j].x,Lpnj[j].y,Lpnj[j].largeur,Lpnj[j].longeur)
        if Lpnj[j].collisionmur(Lmurs)==1:
            Lpnj[j].y-=j1.Lprojs[i].dy*j1.Lprojs[i].degats
        if Lpnj[j].dx==-1 : #regarde si l'ennemi va vers la droite ou la gauche
         Lpnj[j].sprite = Lpnj[j].frame4 #on fait clignoter l'ennemi dans le bon sens
        else:
         Lpnj[j].sprite = Lpnj[j].frame42
        j1.Lprojs[i]=0 #détruit le projectile

def gestion_des_morts (): #gère la disparition des ennemis morts (afin que leur projectiles restants ne disparaissent pas "par magie")
    for i in range (len(Lpnj)): #on recher d'abord les ennemis en eju et sur le point de mourrir (dont la vie égale 0)
     if Lpnj[i]!=0 and Lpnj[i].vie<=0:
      Lpnj[i].x=2000 #on sort l'ennemi de l'écran
      Lpnj[i].dt-=1 #on attend le temps nescessaire pour être sûr que tout ses projectiles soient détruis
      if Lpnj[i].dt==0:
       etage.puissennemis-=Lpnj[i].puissance #on retranche la puissance de l'ennemi au total
       Lpnj[i]=0 #puis on l'élimine définitivement


#Variables et fonctions pour le menu
resolution=1024,704
menu=pygame.transform.scale(menu,resolution)
ecran_selection=pygame.transform.scale(ecran_selection,resolution)
ecran_credits=pygame.transform.scale(ecran_credits,resolution)
histoire=pygame.transform.scale(histoire,resolution)
fleche=pygame.transform.scale(fleche,(60,30))
fen.blit(menu,(0,0))
position_fleche= xfleche, yfleche= int(0.365*1024),int(0.7175*704) #Permet de garder la même position de la fleche même après changement de résolution
rectfleche=fen.blit(fleche,(xfleche,yfleche))
pygame.display.update() #Actualisation interface graphique
choix_menu=0 #Choix
affichage_fleche=1 #Si égale à 1 une fleche est affichée sinon non
fond=menu #Ecran de base = menu
numero_ecran=-1
deplacement_fleche=int(0.04875*704) #Nombre de pixel parcourus par la flèche après un "input"

def actualisation_menu (ArrierePlan): # Fonction d'actualisation
    fen.blit(ArrierePlan,(0,0))
    if affichage_fleche==1:
        fen.blit(fleche,rectfleche)
    pygame.display.update()

def coupesonsmenu ():
    son_valide.stop()
    son_retour.stop()

"""boucle principale menu"""

while lancement==0:

    for evenement in pygame.event.get(): #Detection des évènements
        #Ferme le programme
        if evenement.type==QUIT:
            pygame.quit()
            sys.exit()
        #Si touche pressée
        if evenement.type==KEYDOWN:
            listetouche=pygame.key.get_pressed()   #Identification de la touche pressée
            if 0<=choix_menu<3 and listetouche[K_DOWN] or listetouche[K_s]and choix_menu<3 : #Si touche s ou bas pressée change de choix (n+1)
                choix_menu+=1
                rectfleche.y=rectfleche.y+deplacement_fleche #Bouge la fleche
                coupesonsmenu()
                son_valide.play(0)
            if 3>=choix_menu>0 and listetouche[K_UP] or listetouche[K_w] and choix_menu>0: #Si z ou s pressé change de choix (n-1)
                choix_menu-=1
                rectfleche.y=rectfleche.y-deplacement_fleche #Bouge la fleche
                coupesonsmenu()
                son_valide.play(0)
        if choix_menu==0: #Si la fleche point "Jouer"
            if evenement.type==KEYDOWN:
                    listetouche=pygame.key.get_pressed()
                    if listetouche[K_RETURN]: #Si touche entrée pressée
                        fond=ecran_selection
                        affichage_fleche=0
                        numero_ecran=0
                        intermediaire=choix_menu
                        choix_menu=4
                        coupesonsmenu()
                        son_valide.play(0)
        if choix_menu==1: #Si fleche sur le bouton sélection
            if evenement.type==KEYDOWN:
                listetouche=pygame.key.get_pressed()
                if listetouche[K_RETURN]: #Et si touhe pressée = entrée
                    fond=histoire #Ecran de sélection du personnage
                    affichage_fleche=0
                    numero_ecran=1
                    intermediaire=choix_menu #Conserve choix menu
                    choix_menu=4 #Change choix menu
                    coupesonsmenu()
                    son_valide.play(0)
                     #Réinitialise la variable liste
        if choix_menu==2: #Si fleche sur le bouton credits
            if evenement.type==KEYDOWN:
                listetouche=pygame.key.get_pressed()
                if listetouche[K_RETURN]: # Et si touche pressée = entrée
                    fond=ecran_credits #Ecran des crédits (contributeurs...)
                    affichage_fleche=0
                    numero_ecran=2
                    intermediaire=choix_menu #Conserve la valeur de choix menu
                    choix_menu=4 #Change la valeur de choix menu
                    coupesonsmenu()
                    son_valide.play(0)
        if choix_menu==3: #Si fleche sur le bouton quitter
            if evenement.type==KEYDOWN: #Et touche entrée pressée
                listetouche=pygame.key.get_pressed()
                if listetouche[K_RETURN]:
                    pygame.quit() #Quitte le jeu et ferme la fenetre
                    sys.exit()
        if numero_ecran==0:
                if evenement.type==KEYDOWN:
                    listetouche=pygame.key.get_pressed()
                    if listetouche[K_BACKSPACE]:
                        affichage_fleche=1
                        fond=menu
                        choix_menu=intermediaire
                        coupesonsmenu()
                        son_retour.play(0)
                    if listetouche[K_SPACE]: #si on appuie sur espace, le jeu se lance
                        lancement=1
                        #LE JEU SE LANCE DONC ICI
        if numero_ecran==1:
                if evenement.type==KEYDOWN:
                    listetouche=pygame.key.get_pressed()
                    if listetouche[K_BACKSPACE]: #en appuyant sur retour, on retourne au menu de départ
                        affichage_fleche=1
                        fond=menu
                        choix_menu=intermediaire
                        coupesonsmenu()
                        son_retour.play(0)
        if numero_ecran==2:
                if evenement.type==KEYDOWN:
                    listetouche=pygame.key.get_pressed()
                    if listetouche[K_BACKSPACE]: #en appuyant sur retour, on retourne au menu de départ
                        affichage_fleche=1
                        fond=menu
                        choix_menu=intermediaire
                        coupesonsmenu()
                        son_retour.play(0)
        actualisation_menu(fond) #on actualise


"""boucle principale jeu"""

while lancement==1 :
    temps=pygame.time.get_ticks()
    for evenement in pygame.event.get():

        if evenement.type==QUIT:     #boucle quit
            pygame.quit()
            sys.exit()

        #déplacements
        if evenement.type==KEYDOWN: #détecte le début d'un déplacement
            if evenement.key==K_RIGHT or evenement.key == K_d: #détecte la touche
                    kd=1 #état la touche = 0
            elif evenement.key==K_LEFT or evenement.key== K_a: #idem
                    ka=1
            elif evenement.key==K_UP or evenement.key== K_w:
                    kw=1
            elif evenement.key==K_DOWN or evenement.key== K_s:
                    ks=1
        if evenement.type==KEYUP: #détecte la fin d'un déplacement
            if evenement.key==K_RIGHT or evenement.key == K_d:
                kd=0 #état la touche = 0
            elif evenement.key==K_LEFT or evenement.key== K_a:
                ka=0
            elif evenement.key==K_UP or evenement.key== K_w:
                kw=0
            elif evenement.key==K_DOWN or evenement.key== K_s:
                ks=0
        j1.dx = kd-ka #la valeur de déplacement est changée en fonction des touches appuyées
        j1.dy = ks-kw

        #tirs
        if evenement.type == MOUSEBUTTONDOWN and evenement.button == 1 and j1.Lsorts[j1.sortactuel]!=0: #au clic, on tire (si un sort est choisi)
            j1.tir()
        if evenement.type==KEYDOWN: #avec 1,2,3 on change de sort actif
            if evenement.key==K_1:
                j1.sortactuel=1
            if evenement.key==K_2:
                j1.sortactuel=2
            if evenement.key==K_3:
                j1.sortactuel=3
        #bouclier
        if evenement.type== MOUSEBUTTONDOWN:
            if  evenement.button == 3 and j1.bouclier!=-1: #si le joueur appuie sur le clic droit et que le bouclier n'est pas brisé, il active son bouclier
                j1.bouclier=1
        if evenement.type== MOUSEBUTTONUP:
            if  evenement.button == 3 and j1.bouclier!=-1:
                j1.bouclier=0



#gestion de événements en lien avec le temps
    if(temps-tempsecoule>50): #base : action tous les 50ms
        tempsecoule = temps #on actualise le temps écoulé

        j1.jdeplacements(Lmurs) #déplacements joueur
        if j1.mana+j1.manar<=j1.manamax : #régeneration du mana (sans jamais d'passer le maximum)
            j1.mana+=j1.manar
        else:
            j1.mana=j1.manamax

        for i in range (len(Lpnj)): #gestion des pnj
            if Lpnj[i]!=0: #si l'ennemi existe
             if etage.tpsapparition!=0: #si les ennemis ne sont pas actifs
              etage.tpsapparition-=1 #on réduit le délai
             else: #si les ennemis sont actifs, on cherche l'ia leur correspondant et on l'applique
                if Lpnj[i].ia == 1:
                 Lpnj[i].ia1(j1,Lmurs)
                if Lpnj[i].ia == 12:
                 Lpnj[i].ia12(j1,Lmurs)
                if Lpnj[i].ia == 2:
                 Lpnj[i].ia2(j1,Lmurs)
                if Lpnj[i].ia == 24:
                 Lpnj[i].ia24(j1,Lmurs)
                if Lpnj[i].ia == 242:
                 Lpnj[i].ia242(j1,Lmurs)
                if Lpnj[i].ia == 28:
                 Lpnj[i].ia28(j1,Lmurs)
                if Lpnj[i].ia == 216:
                 Lpnj[i].ia216(j1,Lmurs)
        actualisation_ig() #actualisation des entitées (fonctions déjà definies pour plus de visibilité)
        gestion_des_morts()
        if etage.tpsapparition==0: #si les ennemis sont actifs
            collision_ennemi() #gestion des collisions
            collision_joueur() #gestion des collisions #actualisation des entitées n°2 (on actualise avant les collision pour ne pas avoir de soucis sur le "hitbox")