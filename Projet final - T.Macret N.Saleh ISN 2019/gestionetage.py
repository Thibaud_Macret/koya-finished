﻿import pygame,sys,random,classes
from pygame import *
from classes import *
pygame.init()
fen = pygame.display.set_mode((1024,704))

"""gestion du côté "rogue like" : génération des étages, ennemis..."""

#liste des ennemis possibles :
pennemi0=Pnj(0,0,50,50,"scorpion",38,4,242,22,15,0,20,15,40,1.8)
pennemi1=Pnj(0,0,32,32,"cactus_guerrier",26,3,28,18,17,0,25,14,35,1.6)
pennemi2=Pnj(0,0,32,32,"sorcier",16,3,2,12,24,160,13,13,50,1.9)
pennemi3=Pnj(0,0,32,32,"serpent",10,5,2,18,17,0,25,10,35,1)
pennemi4=Pnj(0,0,32,32,"elem1",8,7,12,19,0,0,0,0,1,1.2)
Lennemis=[pennemi0,pennemi1,pennemi2,pennemi3,pennemi4]
#création de la "porte"
porte=Entité(480,306,64,64,"personnages/portailJ")

class Etage ():
    def __init__(self): #initialisation en début de partie
     self.profondeur = -1
     self.puissennemis = 0 #le total de puissance des ennemis
     self.biome = 1
     self.Lpuiss = [3,5,6,7,11,7,8,9,10,11,9,10,12,14,20] #l'odre de difficultée des salles (il y a 15 salles)
     self.tpsapparition = 0 #la variable servant à déconter le temps d'apparition des ennemis

    def actuetage(self,joueur,Lpnj,Lmurs): #actualise l'étage :
        if self.puissennemis<0.9: #s'il est vide, on crée la porte
            self.puissennemis = 0
            porte.actuetblit()
            if joueur.rect.colliderect(porte): #lorsque l'on prend la porte, on change d'étage
                self.profondeur+=1 #on augmente la profondeur de 1
                if self.profondeur==15:#si le 16eme étage est atteint, c'est gagné
                    prin("GG!")
                    pygame.quit()
                    sys.exit()
                joueur.x=496 #on place le joueur au centre (pour ne pas être bloqué)
                joueur.y=322
                joueur.mana=joueur.manamax #on remonte le mana au max.
                joueur.vie+=int(random.randint(3,6)*joueur.viemax/100) #le joueur regagne, de manière aléatoire de 3% à 6% de vie
                if joueur.vie>joueur.viemax: #(et on empêche le dépacement du maximum)
                 joueur.vie=joueur.viemax
                if self.profondeur%5==0: #un nouveau sort est optenu tous les 5 étages (soit 0,5et10)
                #on crée une liste contenant tous les sorts, pour ne pas prendre le risque d'avoir deux fois le même
                 joueur.Lpossibilitees=[joueur.sort1,joueur.sort2,joueur.sort3,joueur.sort4,joueur.sort5,joueur.sort6]
                 sortt=random.randint(0,len(joueur.Lpossibilitees)-1) #on tire un sort, on l'ajoute à l'arsenal du joueur et le retire aux possibilitées
                 joueur.Lsorts[int(self.profondeur/5)+1]=joueur.Lpossibilitees[sortt]
                 del(joueur.Lpossibilitees[sortt])
                bonus = random.randint(1,6) #à la fin d'un étage, le joueur reçoit un bonus aléatoire
                if bonus==1:#les bonus :
                 joueur.ms+=1 #1 de vitesse de déplacement
                elif bonus==2:
                 joueur.resboucliermax+=5 #capacité maximales de bouclier
                elif bonus==3:
                 joueur.manar+=.1 #régénération de mana augmentée
                else: #sinon, on améliore un sort
                    if joueur.Lsorts[3]!=0: #si on a un sort 3, il est amélioré, sinon le 2, sinon le 1
                     joueur.Lsorts[3].cout,joueur.Lsorts[3].degats = int((joueur.Lsorts[3].cout)*0.9),int((joueur.Lsorts[3].degats)*1.1) #dégâts+10%, cout-10%
                    elif joueur.Lsorts[2]!=0:
                     joueur.Lsorts[2].cout = int((joueur.Lsorts[2].cout)*0.8) #cout-20%
                    else:
                     joueur.Lsorts[1].degats = int((joueur.Lsorts[1].degats)*1.2) #dégâts+20%
                self.creemurs(Lmurs) #puis on crée murs et ennemis
                self.creennemis(Lpnj)


    def creemurs (self,Lmurs):
        for i in range (len(Lmurs)):#on suprime tout les murs (sauf les bordures)
         if i>3:
          Lmurs[i]=0
        self.Lpapparition=[]
        for q in range (4):#on crée l'étage aléatoirement par quart
         fichier = open ("seeds/Etages/"+str(random.randint(1,30))+".txt","r") #on ouvre une graine tirée au hasard
         contenu = fichier.read() #on lis et on met toutes le données dans une liste
         seed = contenu.split()
         if q==1 or q ==3: #calcul du point de départ de ce quart (32 ou 544 pour x, 50 ou 434 pour y)
          bx=544
         else:
          bx=32
         if q==2 or q==3:
          by=370
         else:
          by=50
         for i in range (len(seed)):
          coordx,coordy=i%14*32+bx,int(i/14)*32+by #la liste ayant passé less donnée en "2D" on doit calculer les cordonnées sur un "plan 3D"
          if seed[i]=="M": #si on trouve un mur de base,
           j=0
           while j<(len(Lmurs)): #on le crée
            if Lmurs[j]==0:
             Lmurs[j]=Mur(coordx,coordy,32,32,"decor/desert/mur1"+str(random.randint(1,5)),1)
             j=(len(Lmurs))
            j+=1
          if seed[i]=="m": #si on trouve un mur non solide,
           j=0
           while j<(len(Lmurs)): #même technique
            if Lmurs[j]==0:
             Lmurs[j]=Mur(coordx,coordy,32,32,"decor/desert/mur0"+str(random.randint(1,2)),0)
             j=(len(Lmurs))
            j+=1
          if seed[i]=="4": #si on trouve un mur de 4,
           j=0
           while j<(len(Lmurs)):
            if Lmurs[j]==0:
             Lmurs[j]=Mur(coordx,coordy,64,64,"decor/desert/mur4"+str(random.randint(1,3)),1)
             j=(len(Lmurs))
            j+=1
          if seed[i]=="9": #si on trouve un mur 9,
           j=0
           while j<(len(Lmurs)):
            if Lmurs[j]==0:
             Lmurs[j]=Mur(coordx,coordy,96,96,"decor/desert/mur9"+str(random.randint(1,3)),1)
             j=(len(Lmurs))
            j+=1
          if seed[i]=="E": #si on trouve un point d'apparition,
            self.Lpapparition+=coordx,coordy
         fichier.close()


    def creennemis (self,Lpnj): #cree les ennemis de façon a que leur total de puissance soit égal ou suppérieur de 1 par rapport à la difficultée de l'étage
        self.tpsapparition = 70 #les ennemis ne seront actifs qu'au bout de 3,5 secondes
        while self.puissennemis<self.Lpuiss[self.profondeur]: #tant que le total de difficulté n'est pas dépassé
            nouven = random.randint(0,(len(Lennemis)-1)) #le nouvel ennemi est tiré au sort dans la liste d'ennemis
            self.i=0 #variable pour parcourir la liste des ennemis
            while self.i<(len(Lpnj)):
                if Lpnj[self.i]==0: #si l'emplacement n'est pas pris, on crée l'ennemi
                    a = Lennemis[nouven] #on pose a pour réduire la ligne de création
                    point = random.randint(1,len(self.Lpapparition)/2) #on tire le point d'apparition au hasard
                    x,y = self.Lpapparition[point*2-2],self.Lpapparition[point*2-1]
                    del(self.Lpapparition[point*2-1])
                    del(self.Lpapparition[point*2-2])
                    Lpnj[self.i] = Pnj(x,y,a.largeur,a.longeur,a.nomplanche,a.vie,a.ms,a.ia,a.dmgc,a.dmgd,a.ar,a.va,a.ct,a.dt,a.puissance)
                    self.puissennemis+=Lpnj[self.i].puissance #on actualise le total de puissance
                    self.i+=(len(Lpnj))#on met fin à la recherche
                self.i+=1