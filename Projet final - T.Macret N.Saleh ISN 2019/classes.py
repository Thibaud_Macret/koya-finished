﻿import pygame,sys,random
from pygame import *
pygame.init()
fen = pygame.display.set_mode((1024,704))

"""Toutes les classes"""

class Entité: #tout ce qui sera visible
    def __init__(self,x,y,x_taille,y_taille,sprite):
     self.x = x
     self.y = y
     self.largeur = x_taille
     self.longeur = y_taille
     self.rect = pygame.Rect(x,y,self.largeur,self.longeur)
     self.sprite=pygame.image.load("sprites/"+sprite+".png")

    def actuetblit(self): #actualise la hitbox et l'image
        self.rect=pygame.Rect(int(self.x),int(self.y),int(self.largeur),int(self.longeur)) #on utilie int car un pixel ne peut être coupé en deux
        fen.blit(self.sprite,(int(self.x),int(self.y)))

    def angle(self,cible): #calcule l'angle dans lequel se trouve la cible parmi 8 possibilitées (pour savoir le sens de déplacement/de tir)
        if (self.x > cible.x) :
         self.dx = -1
        if (self.x < cible.x) :
         self.dx = 1
        if (self.y > cible.y) :
         self.dy = -1
        if (self.y < cible.y) :
         self.dy = 1
        if (self.dx!=0) and (self.dy!=0): #dans la majorité des cas, on obtiendras une diagonale, ces lignes permettent "d'arrondir"cela
         if abs(self.x-cible.x)/2 >= abs(self.y-cible.y):
          self.dy=0
         if abs(self.y-cible.y)/2 >= abs(self.x-cible.x):
          self.dx=0

    def collisionmur(self,Lmurs): #détecte une collision avec un mur : retourne 1 en cas de collision
        i=0 #valeur temporaire pour parcourir la liste
        while i!=(len(Lmurs)):
         if Lmurs[i]!=0: #on cherche les murs existants
          if self.rect.colliderect(Lmurs[i]): #et on test la collision
            i=(len(Lmurs))
            return(1)
          else:
            i+=1
         else:
          i+=1



class Sort : #permet de définir les caractéristiques d'un sort
    def __init__ (self,idt,longeur,largeur,sprite,cout,degats,vitesse,ddv,traj,nombre):
     self.idt=idt
     self.longeur = longeur
     self.largeur = largeur
     self.sprite = "personnages/joueur/sort"+sprite
     self.miniature = pygame.image.load("sprites/HUD/sort"+sprite+".png")
     self.cout = cout
     self.degats = degats
     self.vitesse = vitesse
     self.ddv = ddv #(durée de vie)
     self.traj = traj #le type de trajerctoire (0=droite, 2=ondulante, ...)
     self.nombre = nombre #(de projectiles)



class Joueur (Entité): #le joueur
    def __init__(self,x,y,x_taille,y_taille,sprite,vie,mana,manar,tbouclier,ms):
     self.x = x
     self.y = y
     self.largeur = x_taille
     self.longeur = y_taille #coordonnées de l'entitée
     self.rect = pygame.Rect(self.x,self.y,self.largeur,self.longeur)
     self.planche=pygame.image.load("sprites/personnages/joueur/"+sprite+".png") #gestion du visuel de l'entitée : on découpe par image
     self.frame01 = self.planche.subsurface((0,0,self.largeur,self.longeur))
     self.sprite = self.frame01 #on découpe la planche en 20 frames : 5 par côté : les 4 premières sont l'animation de base et la cinquième fait "clignoter"
     self.etatframe = 1 #et on utilise cette variable pour gérer quelle frame doit être affichée
     self.frame02 = self.planche.subsurface((0,self.longeur*1,self.largeur,self.longeur))
     self.frame03 = self.planche.subsurface((0,self.longeur*2,self.largeur,self.longeur))
     self.frame04 = self.planche.subsurface((0,self.longeur*3,self.largeur,self.longeur))
     self.frame12 = self.planche.subsurface((self.largeur,0,self.largeur,self.longeur))
     self.frame22 = self.planche.subsurface((self.largeur,self.longeur*1,self.largeur,self.longeur))
     self.frame32 = self.planche.subsurface((self.largeur,self.longeur*2,self.largeur,self.longeur))
     self.frame42 = self.planche.subsurface((self.largeur,self.longeur*3,self.largeur,self.longeur))
     self.frame13 = self.planche.subsurface((self.largeur*2,0,self.largeur,self.longeur))
     self.frame23 = self.planche.subsurface((self.largeur*2,self.longeur*1,self.largeur,self.longeur))
     self.frame33 = self.planche.subsurface((self.largeur*2,self.longeur*2,self.largeur,self.longeur))
     self.frame43 = self.planche.subsurface((self.largeur*2,self.longeur*3,self.largeur,self.longeur))
     self.frame51 = self.planche.subsurface((self.largeur*3,0,self.largeur,self.longeur))
     self.frame52 = self.planche.subsurface((self.largeur*3,self.longeur*1,self.largeur,self.longeur))
     self.frame53 = self.planche.subsurface((self.largeur*3,self.longeur*2,self.largeur,self.longeur))
     self.frame54 = self.planche.subsurface((self.largeur*3,self.longeur*3,self.largeur,self.longeur))
     self.frame_bou = self.planche.subsurface((self.largeur*4,0,self.largeur,self.longeur))
     self.vie = vie
     self.viemax = vie #paramètres de base vie, son maximum, le mana...
     self.mana = mana
     self.manar = manar #régeneration du mana (pour 50ms)
     self.manamax = mana
     self.invu = 0 #l'état d'invulnérabilité du personnage
     self.ms = ms #vitesse de déplacement
     self.dx = 0 #direction en x
     self.dy = 0 #en y
     self.bouclier=0 #l'état du bouclier 'donc nul au début
     self.resbouclier = tbouclier #la résistance du bouclier en(sur une base de 50ms)
     self.resboucliermax = tbouclier
        #création des emplacements à sorts
     self.sort1,self.sort2,self.sort3,self.sort4 = 0,0,0,0
     self.Lsorts = [0,self.sort1,self.sort2,self.sort3,self.sort4]
     self.sortactuel = 1
        #création des emplacements à projectiles et leur regroupement
     self.proj1,self.proj2,self.proj3,self.proj4,self.proj5,self.proj6,self.proj7,self.proj8,self.proj9,self.proj10 = 0,0,0,0,0,0,0,0,0,0
     self.proj11,self.proj12,self.proj13,self.proj14,self.proj15,self.proj16,self.proj17,self.proj18,self.proj19,self.proj20 = 0,0,0,0,0,0,0,0,0,0
     self.proj21,self.proj22,self.proj23,self.proj24,self.proj25,self.proj26,self.proj27,self.proj28,self.proj29,self.proj30 = 0,0,0,0,0,0,0,0,0,0
     self.proj31,self.proj32,self.proj33,self.proj34,self.proj35,self.proj36,self.proj37,self.proj38,self.proj39,self.proj40 = 0,0,0,0,0,0,0,0,0,0
     self.proj41,self.proj42,self.proj43,self.proj44,self.proj45,self.proj46,self.proj47,self.proj48,self.proj49,self.proj50 = 0,0,0,0,0,0,0,0,0,0
     self.proj51,self.proj52,self.proj53,self.proj54,self.proj55,self.proj56,self.proj57,self.proj58,self.proj59,self.proj60 = 0,0,0,0,0,0,0,0,0,0
     self.Lprojs = [self.proj1,self.proj2,self.proj3,self.proj4,self.proj5,self.proj6,self.proj7,self.proj8,self.proj9,self.proj10]
     self.Lprojs+= self.proj11,self.proj12,self.proj13,self.proj14,self.proj15,self.proj16,self.proj17,self.proj18,self.proj19,self.proj20
     self.Lprojs+= self.proj21,self.proj22,self.proj23,self.proj24,self.proj25,self.proj26,self.proj27,self.proj28,self.proj29,self.proj30
     self.Lprojs+= self.proj31,self.proj32,self.proj33,self.proj34,self.proj35,self.proj36,self.proj37,self.proj38,self.proj39,self.proj40
     self.Lprojs+= self.proj41,self.proj42,self.proj43,self.proj44,self.proj45,self.proj46,self.proj47,self.proj48,self.proj49,self.proj50
     self.Lprojs+= self.proj51,self.proj52,self.proj53,self.proj54,self.proj55,self.proj56,self.proj57,self.proj58,self.proj59,self.proj60

    def jdeplacements(self,Lmurs): #déplace le joueur
        for i in range (self.ms): #on déplace ms fois de 1px
         if self.bouclier!=1: #en mode bouclier, il est impossible de se déplacer
          self.x+=self.dx
          self.rect=pygame.Rect(self.x,self.y,self.largeur,self.longeur)
          if (self.collisionmur(Lmurs))==1:#en cas de collision, on recule le joueur
           self.x-=self.dx
          self.y+=self.dy
          self.rect=pygame.Rect(self.x,self.y,self.largeur,self.longeur)
         if (self.collisionmur(Lmurs))==1:
          self.y-=self.dy
        #actualisation de l'image :
        if self.etatframe==1:
         self.etatframe=2
         if self.dx==1:
          self.sprite=self.frame03
         elif self.dx==-1:
          self.sprite=self.frame04
         elif self.dy==-1:
          self.sprite=self.frame02
         else:
          self.sprite=self.frame01
        elif self.etatframe==2:
         self.etatframe=3
         if self.dx==1:
          self.sprite=self.frame32
         elif self.dx==-1:
          self.sprite=self.frame42
         elif self.dy==-1:
          self.sprite=self.frame22
         else:
          self.sprite=self.frame12
        elif self.etatframe==3:
         self.etatframe=4
         if self.dx==1:
          self.sprite=self.frame33
         elif self.dx==-1:
          self.sprite=self.frame43
         elif self.dy==-1:
          self.sprite=self.frame23
         else:
          self.sprite=self.frame13
        elif self.etatframe==4:
         self.etatframe=1
         if self.dx==1:
          self.sprite=self.frame53
         elif self.dx==-1:
          self.sprite=self.frame54
         elif self.dy==-1:
          self.sprite=self.frame52
         else:
          self.sprite=self.frame51
        if self.bouclier==1: #si bouclier, l'image est différente
         self.sprite=self.frame_bou


    def actubarre(self,valeur,maxi,sprite,x,y): #actualise les barres du HUD
        carré=pygame.image.load("sprites/HUD/"+sprite+".png")
        for i in range (int(valeur)): #on affiche 1 pixel par unitée présente
            fen.blit(carré,(x+1*i,y))
            #on crée le fond de la barre
        bfond1=pygame.image.load("sprites/HUD/debut.png")
        bfond2=pygame.image.load("sprites/HUD/segment.png")
        bfond3=pygame.image.load("sprites/HUD/fin.png")
        nbsegments = (maxi/50)-2
        fen.blit(bfond1,(x-2,y-2))
        fen.blit(bfond3,(x+(50*(nbsegments+1))-2,y-2)) #50*(nbsegments+1) car on décale de 50 par segment + 50 pour le début
        for i in range(int(nbsegments)):
            fen.blit(bfond2,(x+50*(i+1),y-2))

    def tir(self): #fonction qui sera activée au clic permettant de tirer un projectile
     if self.mana>=self.Lsorts[self.sortactuel].cout:
       self.mana-=self.Lsorts[self.sortactuel].cout #paie le cout en mana
       xsouris,ysouris=pygame.mouse.get_pos() #afin de déterminer le vecteur directionel du projectile, on comparre la position du curseur avec celle du personnage
       vectx,vecty = (xsouris-self.x),(ysouris-self.y) #on calcule le vecteur entre le curseur et le joueur
       if abs(vectx)>=abs(vecty): #on divise par le vecteur ayant la plus grosse valeure absolue afin d'avoir des valeurs entre 0 et 1
        vecty/=abs(vectx)
        vectx/=abs(vectx) #ce calcul donne 1 ou -1, selon le signe de vectx
       else:
        vectx/=abs(vecty)
        vecty/=abs(vecty)
       i=0#variable temporaire pour parcourir la liste
       while i!=(len(self.Lprojs)):
        if self.Lprojs[i]==0 : #recherche le premier projectile inutilisé
         s = self.Lsorts[self.sortactuel] #crée un projectile au centre du tireur correspondant au sort choisi (pour réduire les lignes)
         if s.nombre==1:
          self.Lprojs[i] = Projectile(self.x+(self.largeur/2),self.y+(self.longeur/2),s.largeur,s.longeur,s.sprite,s.vitesse,vectx,vecty,s.traj,s.ddv,s.degats)
         if s.nombre==3: #dans le cas d'un attaque à 3 projectiles, on crée les projectiles (avec un décallage)
          self.Lprojs[i] = Projectile(self.x+(self.largeur/2),self.y+(self.longeur/2),s.largeur,s.longeur,s.sprite,s.vitesse,vectx,vecty,s.traj,s.ddv,s.degats)
          self.Lprojs[i+1] = Projectile(self.x+(self.largeur/2)+(1-abs(vectx))*5*s.largeur,self.y+(self.longeur/2)-(1-abs(vecty))*5*s.longeur,s.largeur,s.longeur,s.sprite,s.vitesse,vectx,vecty,s.traj,s.ddv,s.degats)
          self.Lprojs[i+2] = Projectile(self.x+(self.largeur/2)-(1-abs(vectx))*5*s.largeur,self.y+(self.longeur/2)+(1-abs(vecty))*5*s.longeur,s.largeur,s.longeur,s.sprite,s.vitesse,vectx,vecty,s.traj,s.ddv,s.degats)
         i=(len(self.Lprojs)) #met fin à la boucle (pour ne créer qu'un unique projectile)
        else :
         i+=1

    def actuprojectiles(self): #actualise les porjectiles
        for i in range (len(self.Lprojs)):
            if self.Lprojs[i] != 0:
             p = self.Lprojs[i] #pour gagner en visibilité, on crée un racourci vers self.Lprojs[i]
             if p.special==0: #sauf exeption,
                if self.Lprojs[i].dx!=0: #s'il y a mouvement en x...
                 self.Lprojs[i].x+=p.dx*p.vitesse #on déplace de la vitesse dans la direction donné
                if p.dy!=0: #idem en y
                 self.Lprojs[i].y+=p.dy*p.vitesse
                self.Lprojs[i].ddv+=-1 #on gère la disparition
                if self.Lprojs[i].ddv==0:
                 self.Lprojs[i]=0

             elif p.special==2: #exeption : mouvement odulatoire
                if self.Lprojs[i].dx!=0: #principe : on se base sur la trajectoire droite et on effectue un déclement en quatres temps
                 if self.Lprojs[i].tpspecial==-1: #la première fois, on copie la trajectoire
                  self.Lprojs[i].xdir = self.Lprojs[i].x
                  self.Lprojs[i].ydir = self.Lprojs[i].y
                 self.Lprojs[i].xdir+=p.dx*p.vitesse #on modifie la trajectoire directrice
                 self.Lprojs[i].ydir+=p.dy*p.vitesse
                 if self.Lprojs[i].tpspecial<=3: #premier quart :
                  self.Lprojs[i].tpspecial+=1
                  self.Lprojs[i].x= self.Lprojs[i].xdir+self.Lprojs[i].tpspecial*10*p.dy
                  self.Lprojs[i].y= self.Lprojs[i].ydir-self.Lprojs[i].tpspecial*10*p.dx
                 elif self.Lprojs[i].tpspecial<=6: #deuxième quart :
                  self.Lprojs[i].tpspecial+=1
                  self.Lprojs[i].x= self.Lprojs[i].xdir+(6-self.Lprojs[i].tpspecial)*10*p.dy
                  self.Lprojs[i].y= self.Lprojs[i].ydir-(6-self.Lprojs[i].tpspecial)*10*p.dx
                 elif self.Lprojs[i].tpspecial<=9: #troisième quart :
                  self.Lprojs[i].tpspecial+=1
                  self.Lprojs[i].x= self.Lprojs[i].xdir-(self.Lprojs[i].tpspecial-10)*10*p.dy
                  self.Lprojs[i].y= self.Lprojs[i].ydir+(self.Lprojs[i].tpspecial-10)*10*p.dx
                 else: #dernier quart :
                  self.Lprojs[i].tpspecial+=1
                  self.Lprojs[i].x= self.Lprojs[i].xdir-(12-self.Lprojs[i].tpspecial)*10*p.dy
                  self.Lprojs[i].y= self.Lprojs[i].ydir+(12-self.Lprojs[i].tpspecial)*10*p.dx
                  if self.Lprojs[i].tpspecial==12: #fin : on retourne à 0
                    self.Lprojs[i].tpspecial=0
                self.Lprojs[i].ddv+=-1 #gestion de la disparition
                if self.Lprojs[i].ddv==0:
                 self.Lprojs[i]=0

             elif p.special==3:#exeption 2 : boomerang
                if self.Lprojs[i].dx!=0: #principe : on avance pendant t/2 puis on recule
                 if self.Lprojs[i].tpspecial==-1:
                  self.Lprojs[i].tpspecial=0
                 if self.Lprojs[i].tpspecial<(self.Lprojs[i].ddv/2):
                    self.Lprojs[i].tpspecial+=1
                 else:
                    self.Lprojs[i].dx/=-1
                    self.Lprojs[i].dy/=-1
                    self.Lprojs[i].tpspecial=0
                if self.Lprojs[i].dx!=0: #on gère ensuite le projectile normalement
                 self.Lprojs[i].x+=p.dx*p.vitesse #on déplace de la vitesse dans la direction donné
                if p.dy!=0:
                 self.Lprojs[i].y+=p.dy*p.vitesse
                self.Lprojs[i].ddv+=-1 #on gère la disparition
                if self.Lprojs[i].ddv==0:
                 self.Lprojs[i]=0

    def actubouclier(self): #actualise le bouclier
        bouclier=pygame.image.load("sprites/personnages/joueur/shield.png")
        if self.bouclier==1 and self.resbouclier>=0: #tant que le joueur utilise son bouclier, la résistance diminue
            self.resbouclier-=1
        if self.resbouclier<=0: #si la résistance atteint 0, le bouclier est brisé
            self.bouclier=-1
        #actualisation du marqueur de bouclier
        if self.resbouclier/self.resboucliermax<=0.25 :
            fen.blit(bouclier.subsurface(0,96,32,32),(512,10))
        elif self.resbouclier/self.resboucliermax<=0.50:
            fen.blit(bouclier.subsurface(0,64,32,32),(512,10))
        elif self.resbouclier/self.resboucliermax<=0.75:
            fen.blit(bouclier.subsurface(0,32,32,32),(512,10))
        else:
            fen.blit(bouclier.subsurface(0,0,32,32),(512,10))
        if self.bouclier==-1:
            fen.blit(bouclier.subsurface(0,128,32,32),(512,10))

        if self.resbouclier<self.resboucliermax: #si la résistance n'es pas au maximum, on le régénère
            self.resbouclier+=0.35
        if self.resbouclier>=self.resboucliermax and self.bouclier==-1: # si le bouclier s'est brisé et qu'il est rechargé, on le débloque
            self.bouclier = 0
            self.resbouclier=self.resboucliermax

    def actusorts(self): #actualise le HUD relatif aux sorts
        cadrenoir=pygame.image.load("sprites/HUD/cadresort0.png")
        cadreblanc=pygame.image.load("sprites/HUD/cadresort1.png")
        vide=pygame.image.load("sprites/HUD/sort0.png")
        for i in range (3):
         fen.blit(cadrenoir,(92+74*i,633)) #crée le tour des cadres
        if self.Lsorts[1]!=0: #pour chaque sort, on affiche l'îcone correspondant
         fen.blit(self.Lsorts[1].miniature,(94,635))
        else :
         fen.blit(vide,(94,635))
        if self.Lsorts[2]!=0:
         fen.blit(self.Lsorts[2].miniature,(168,635))
        else :
         fen.blit(vide,(168,635))
        if self.Lsorts[3]!=0:
         fen.blit(self.Lsorts[3].miniature,(242,635))
        else :
         fen.blit(vide,(242,635))
        fen.blit(cadreblanc,(92+74*(self.sortactuel-1),633)) #on entoure de blanc le sort choisi




class Pnj (Entité): #les PNJ (=ennemis)
    def __init__(self,x,y,x_taille,y_taille,sprite,vie,ms,ia,dmgc,dmgd,ar,va,ct,dt,puissance):
     self.x = x
     self.y = y
     self.largeur = x_taille
     self.longeur = y_taille
     self.rect = pygame.Rect(self.x,self.y,self.largeur,self.longeur)
     self.nomplanche = sprite
     self.planche = pygame.image.load("sprites/personnages/ennemis/"+sprite+".png")
     self.frame1 = self.planche.subsurface((0,0,self.largeur,self.longeur))
     self.sprite = self.frame1 #on découpe la planche en 8 frames : 4 par côté : les 3 premières sont l'animation de base et la quatrième fait "clignoter" le pnj
     self.etatframe = 1 #et on utilise cette variable pour gérer quelle frame doit être affichée
     self.frame2 = self.planche.subsurface((0,self.longeur*1,self.largeur,self.longeur))
     self.frame3 = self.planche.subsurface((0,self.longeur*2,self.largeur,self.longeur))
     self.frame4 = self.planche.subsurface((0,self.longeur*3,self.largeur,self.longeur))
     self.frame12 = self.planche.subsurface((self.largeur,0,self.largeur,self.longeur))
     self.frame22 = self.planche.subsurface((self.largeur,self.longeur*1,self.largeur,self.longeur))
     self.frame32 = self.planche.subsurface((self.largeur,self.longeur*2,self.largeur,self.longeur))
     self.frame42 = self.planche.subsurface((self.largeur,self.longeur*3,self.largeur,self.longeur))
     self.vie = vie
     self.ms = ms #vitesse de déplacement, en px par 50ms
     self.dx = 0 #direction en x
     self.dy = 0 #en y
     self.ia = ia #type d'intelligence artificielle (voir plus bas)
     self.iat = 0 #variable pour tester l'état de l'ennemi (utile pour certains types d'ia)
     self.dmgc = dmgc #dommages au corps à coprs
     self.dmgd = dmgd #dommages à distance
     self.ar = ar #agro range (portée d'agression) en px
     self.va = va #vitesse d'attaque (=délai entre deux attaques, en 50ms)
     self.tempstir = 0 #la variable qui calculera le délai avant le prochain tir
     self.ct = ct #vitesse du tir (du projectile)
     self.dt = dt #la durée de vie des projectiles
     self.proj1,self.proj2,self.proj3,self.proj4,self.proj5,self.proj6,self.proj7,self.proj8,self.proj9,self.proj10 = 0,0,0,0,0,0,0,0,0,0 #création des emplacements à projectiles
     self.proj11,self.proj12,self.proj13,self.proj14,self.proj15,self.proj16,self.proj17,self.proj18,self.proj19,self.proj20 = 0,0,0,0,0,0,0,0,0,0
     self.proj21,self.proj22,self.proj23,self.proj24,self.proj25,self.proj26,self.proj27,self.proj28,self.proj29,self.proj30 = 0,0,0,0,0,0,0,0,0,0
     self.Lprojs = [self.proj1,self.proj2,self.proj3,self.proj4,self.proj5,self.proj6,self.proj7,self.proj8,self.proj9,self.proj10] #...et leur regroupement
     self.Lprojs+= self.proj11,self.proj12,self.proj13,self.proj14,self.proj15,self.proj16,self.proj17,self.proj18,self.proj19,self.proj20
     self.Lprojs+= self.proj21,self.proj22,self.proj23,self.proj24,self.proj25,self.proj26,self.proj27,self.proj28,self.proj29,self.proj30
     self.puissance = puissance #sert pour la génération au début de chaque salle


    def distance(self,cible,rayon): #calcule la distance entre deux entitées et les compare à un rayon donné
        dist = ((self.x-cible.x)**2+(self.y-cible.y)**2)**0.5 #calcul de la distance
        if dist == rayon:
            dist=0 #revoie 0 pour un disance égale
        elif dist > rayon :
            dist=1 #1 pour une distance suppérieure
        elif dist < rayon :
            dist=-1 #-1 pour une distance inférieure
        return(dist)

    def deplacement(self,cible,Lmurs): #base pour les déplacements
        self.angle(cible)
        for i in range (self.ms):
            self.x+=self.dx*self.distance(cible,self.ar) #on déplace en x
            self.rect=pygame.Rect(self.x,self.y,self.largeur,self.longeur) #on s'assure qu'il n'y a pas de murs
            if self.collisionmur(Lmurs)==1:
             self.x-=self.dx*self.distance(cible,self.ar)
            self.y+=self.dy*self.distance(cible,self.ar)
            self.rect=pygame.Rect(self.x,self.y,self.largeur,self.longeur)
            if self.collisionmur(Lmurs)==1:
             self.y-=self.dy*self.distance(cible,self.ar)
        if self.etatframe==1: #actualisation de l'image (permet l'animation des personnages)
         self.etatframe+=1
         if self.dx==-1 : #regarde si l'ennemi va vers la droite ou la gauche
          self.sprite = self.frame1
         else:
          self.sprite = self.frame12
        elif self.etatframe==2:
         self.etatframe+=1
         if self.dx==-1 :
          self.sprite = self.frame2
         else:
          self.sprite = self.frame22
        elif self.etatframe==3:
         self.etatframe=1
         if self.dx==-1 :
          self.sprite = self.frame3
         else:
          self.sprite = self.frame32

    def tirc(self,cible): #tir ciblé: un projectile vers un cible tous les délais de tirs (va)
            i=0#variable temporaire pour parcourir la liste
            while i!=(len(self.Lprojs)):
                if self.Lprojs[i]==0 : #recherche le premier projectile inutilisé
                    vectx,vecty = (cible.x-self.x),(cible.y-self.y) #on calcule le vecteur entre l'ennemi et le joueur
                    if abs(vectx)>=abs(vecty): #on divise par le vecteur ayant la plus grosse valeure absolue afin d'avoir des valeurs entre 0 et 1
                        if vectx==0: #comme on va diviser par vectx, on s'assure qu'il soit non nul
                         vectx=0.0001
                        vecty/=abs(vectx)
                        vectx/=abs(vectx)
                    else:
                        vectx/=abs(vecty)
                        vecty/=abs(vecty)
                    self.Lprojs[i] = Projectile(self.x+(self.largeur/2),self.y+(self.longeur/2),10,10,"personnages/ennemis/projennemi",self.ct,vectx,vecty,0,self.dt,self.dmgd)
                    i=(len(self.Lprojs)) #met fin à la boucle (pour ne créer qu'un unique projectile)
                else :
                    i+=1

    def tirb(self,dx,dy): #tir basique : tire dans un direction donnée
        i=0 #variable pour parcourir la liste
        while i != (len(self.Lprojs)): #on parcours la liste
            if self.Lprojs[i] == 0: #au premier projectile disponible, on crée un nouveau
                self.Lprojs[i] = Projectile(self.x+(self.largeur/2),self.y+(self.longeur/2),10,10,"personnages/ennemis/projennemi",self.ct,dx,dy,0,self.dt,self.dmgd)
                i = len(self.Lprojs) #puis on arrête le parcours de la liste
            else:
                i+=1

    def tirm (self,multiple,x,y): #effectue [multiple] tirs à égale distance en partant du point (x,y) (pour x et y positifs et inférieurs à 1)
        sensx,sensy = -1,1 # on tourne dans le sens trigonométrique direct
        for i in range (multiple):
            self.tirb(x,y)
            if abs(x+sensx/(multiple/4))<=1: #si le point suivant est sur le cercle (pour les x)
                x+= sensx/(multiple/4) #on passe au point suivant
            else : #si le point suivant est au bord ou hors du cercle (pour les x)
                sensx/=-1 #on change de sens
            if abs(x)==1:
                sensx/=-1
            if abs(y+sensy/(multiple/4))<=1: #idem pour les y
                y+= sensy/(multiple/4)
            else:
                sensy/=-1
            if abs(y)==1:
                sensy/=-1

    def actuprojs (self): #actualise les projectiles :
        for i in range (len(self.Lprojs)):
            if self.Lprojs[i] != 0: #on parcourt la liste à la recherche des projectiles
                for j in range (self.Lprojs[i].vitesse):
                    self.Lprojs[i].x+=self.Lprojs[i].dx #on le déplace de sa vitesse
                    self.Lprojs[i].y+=self.Lprojs[i].dy
                self.Lprojs[i].ddv-=1 #on diminue sa durée de vie
                if self.Lprojs[i].ddv<=0:
                    self.Lprojs[i]=0 #si cette dernière égale 0, on détruit le projectile



    def ia1 (self,cible,Lmurs): #ia1 = ia qui se contente de se déplacer
        self.deplacement(cible,Lmurs)

    def ia12 (self,cible,Lmurs): #variante : quand le joueur se trouve à moins de 192px, double la vitesse du monstre
        self.deplacement(cible,Lmurs)
        if self.distance(cible,192) != 1 and self.iat != 1 : #si le joueur est dans le rayon et la vitesse n'es pas doublée
            self.iat = 1 #indique que la vitesse est doublée
            self.ms=int(self.ms*2)
        elif self.iat ==1:
            self.iat = 0 #indique que la vitesse est normale
            self.ms=int(self.ms/2)

    def ia2 (self,cible,Lmurs): #ia2 = tireur: se tient à distance de joueur et envoie des projectiles
        self.deplacement(cible,Lmurs)
        if (self.tempstir!=self.va): #test si le délai d'attaque est atteint
            self.tempstir+=1
        else:
            self.tempstir=0
            self.tirc(cible) #tire vers le joueur
        self.actuprojs() #actualisation des projectiles

    def ia24 (self,cible,Lmurs): #variante : tire 4 projectiles dans les directions parrallèles aux axes
        self.deplacement(cible,Lmurs)
        if self.tempstir!=self.va : #test si le délai d'attaque est atteint
            self.tempstir+=1
        else:
            self.tempstir=0
            self.tirm(4,1,0) #effectue 4 tirs orthogonaux
        self.actuprojs() # actualise les projectiles

    def ia242 (self,cible,Lmurs): #variante seconde : tire 4 projectiles dans les directions parrallèles aux médiatrices
        self.deplacement(cible,Lmurs)
        if self.tempstir!=self.va : #test
            self.tempstir+=1
        else:
            self.tempstir=0
            self.tirm(4,.5,.5) #tirs
        self.actuprojs() #actualisation

    def ia28 (self,cible,Lmurs): #variante : fusion de 24 et 242
        self.deplacement(cible,Lmurs)
        if self.tempstir!=self.va :
            self.tempstir+=1
        else:
            self.tempstir=0
            self.tirm(8,1,0) #effectue 8 tirs
        self.actuprojs()

    def ia216 (self,cible,Lmurs): #variante : tire 16 projectiles
        self.deplacement(cible,Lmurs)
        if self.tempstir!=self.va :
            self.tempstir+=1
        else:
            self.tempstir=0
            self.tirm(16,1,0) #effectue les 16 tirs
        self.actuprojs()


class Projectile(Entité):
    def __init__(self,x,y,x_taille,y_taille,sprite,vitesse,vectx,vecty,special,temps,degats):
     self.x = x
     self.y = y
     self.largeur = x_taille
     self.longeur = y_taille
     self.rect = pygame.Rect(self.x,self.y,self.largeur,self.longeur)
     self.sprite = pygame.image.load("sprites/"+sprite+".png")
     self.vitesse = vitesse
     self.dx = vectx #direction en x
     self.dy = vecty #en y
     self.special = special #si le projectile a un attribut particulier (pour 2, sa trajectoire ondule par exemple).
     self.tpspecial = -1 #si besoin, une valeur qui compte le temps
     self.ddv = temps #durée de vie
     self.degats = degats



class Mur(Entité):
    def __init__(self,x,y,x_taille,y_taille,sprite,solidite):
     self.x = x
     self.y = y
     self.largeur = x_taille
     self.longeur = y_taille
     self.rect = pygame.Rect(self.x,self.y,self.largeur,self.longeur)
     self.sprite = pygame.image.load("sprites/"+sprite+".png")
     self.solidite = solidite #si cette valeur est 0, les projectiles peuvent le traverser


